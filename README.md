# Host
https://react-http-2dc58.web.app/

# Installing

Install this app using a command npm i && npm run start.

## Adding a task

You can add a new task by typing in the input and pressing the Enter button on your keyboard.

## Completing a task

You can complete a task by clicking on it.

## Revoke completion

You can revoke a task completion by clicking on it once again. It will be removed to the active tasks pool.

## Filters

You can filter tasks by active, completed or all.

## Clear completed tasks

You can clear completed tasks by pressing the button "Clear completed". It will remove the completed tasks permanently.

## Screenshots

### All tasks filter
![All tasks filter](./screenshots/todos.PNG)
### Active tasks filter
![Active tasks filter](./screenshots/todos1.PNG)
### Completed tasks filter
![Completed tasks filter](./screenshots/todos2.PNG)
