import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Actions } from './components/Actions/Action';
import { Header } from './components/Header/Header';
import { Section } from './components/Section/Section';
import { getAllTasks, saveTask } from './components/store/tasks-actions';
import { Tasks } from './components/Tasks/Tasks';
import { TasksForm } from './components/TasksForm/TasksForm';

let isInitial = true;

function App() {
  const currentItems = useSelector(state => state.tasks.currentItems);
  const activeTasks = useSelector(state => state.tasks.activeTasks);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllTasks());
  }, [dispatch]);

  useEffect(() => {
    if (isInitial) {
      isInitial = false;
      return;
    }

    dispatch(saveTask(currentItems, activeTasks));
  }, [dispatch, currentItems, activeTasks]);

  return (
    <div className="App">
      <Header />
      <Section>
        <TasksForm />
        <Tasks />
        <Actions />
      </Section>
    </div>
  );
}

export default App;
