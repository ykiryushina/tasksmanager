import { memo, useCallback } from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { tasksActions } from '../store/tasks-slice';
import { Button } from '../UI/Button/Button';
import { Wrapper } from '../UI/Wrapper/Wrapper';
import s from './Actions.module.css';

export const Actions = memo(() => {
    const isCompletedFilterActive = useSelector(state => state.tasks.isCompletedFilterActive);
    const isActiveFilterActive = useSelector(state => state.tasks.isActiveFilterActive);
    const activeTasks = useSelector(state => state.tasks.activeTasks);
    const [isAllActive, setIsAllActive] = useState(true);
    const dispatch = useDispatch();

    const filterCompleted = useCallback(() => {
        dispatch(tasksActions.filterTasks('completed'));
        setIsAllActive(false);
    }, [dispatch]);

    const filterActiveTasks = useCallback(() => {
        dispatch(tasksActions.filterTasks('active'));
        setIsAllActive(false);
    }, [dispatch]);

    const clearCompletedTasks = useCallback(() => {
        dispatch(tasksActions.clearCompleted());
    }, [dispatch]);

    const showAllTasks = useCallback(() => {
        dispatch(tasksActions.filterTasks());
        setIsAllActive(true);
    }, [dispatch]);

    return (
        <Wrapper>
            <div className={s.actions}>
                <div className={s.left}>{activeTasks} items left</div>
                <div className={s.filters}>
                    <Button text={'All'} onClick={showAllTasks} isActive={isAllActive} />
                    <Button text={'Active'} onClick={filterActiveTasks} isActive={isActiveFilterActive} />
                    <Button text={'Completed'} onClick={filterCompleted} isActive={isCompletedFilterActive} />
                </div>
                <Button text={'Clear completed'} onClick={clearCompletedTasks} />
            </div>
        </Wrapper>
    )
}) 