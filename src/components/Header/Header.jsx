import s from './Header.module.css';

export const Header = () => {
    return (
        <header className={s.header}>
            <h1 className={s.title}>TODOS</h1>
        </header>
    )
}