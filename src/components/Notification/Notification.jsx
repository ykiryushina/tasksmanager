import { memo } from 'react';
import { Wrapper } from '../UI/Wrapper/Wrapper';
import s from './Notification.module.css';

export const Notification = memo(({ text }) => {
    return (
        <Wrapper>
            <p className={s.notification}>{text}</p>
        </Wrapper>
    )
}) 