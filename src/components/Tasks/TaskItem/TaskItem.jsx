import { useDispatch } from 'react-redux';
import { tasksActions } from '../../store/tasks-slice';
import s from './TaskItem.module.css';

export const TaskItem = ({ text, id, isCompleted }) => {
    const dispatch = useDispatch();

    const completeTask = () => {
        dispatch(tasksActions.completeTask(id));
    }
    const taskClasses = isCompleted ? `${s.task} ${s.completed}` : `${s.task}`

    return (
        <li className={taskClasses} onClick={completeTask}>
            <button className={s.status}>
                {isCompleted &&
                    <svg className={s.check} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="green">
                        <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                    </svg>
                }
            </button>
            <p className={s.text}>{text}</p>
        </li>
    )
}