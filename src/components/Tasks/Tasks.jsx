import { useEffect } from 'react';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { Notification } from '../Notification/Notification';
import { Wrapper } from '../UI/Wrapper/Wrapper';
import { TaskItem } from './TaskItem/TaskItem';

export const Tasks = () => {
    const isCompletedFilterActive = useSelector(state => state.tasks.isCompletedFilterActive);
    const isActiveFilterActive = useSelector(state => state.tasks.isActiveFilterActive);
    const currentTasks = useSelector(state => state.tasks.currentItems);
    const tasks = useSelector(state => state.tasks.items);
    const [notification, setNotification] = useState('');

    useEffect(() => {
        if (!tasks || tasks.length === 0) {
            setNotification('No tasks yet!');
        } else if (isActiveFilterActive) {
            setNotification('You have no active tasks!');
        } else if (isCompletedFilterActive) {
            setNotification('You have no completed tasks!');
        }
    }, [tasks, isActiveFilterActive, isCompletedFilterActive]);

    return (
        <Wrapper>
            {currentTasks.length === 0 && <Notification text={notification} />}
            {
                currentTasks.map(task => (
                    <TaskItem text={task.text} id={task.id} key={task.id} isCompleted={task.isCompleted} />
                ))
            }
        </Wrapper>
    )
}