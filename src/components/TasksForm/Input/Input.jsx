import { useEffect } from 'react';
import { useState } from 'react';
import { Wrapper } from '../../UI/Wrapper/Wrapper';
import s from './Input.module.css';

export const Input = ({ getInputData }) => {
    const [enteredValue, setEnteredValue] = useState('');
    const [isTouched, setIsTouched] = useState(false);

    const valueIsValid = enteredValue.trim().length > 0;
    const hasError = !valueIsValid && isTouched;

    const onValueChangeHandler = (e) => {
        setEnteredValue(e.target.value);
    };

    const onInputHandler = (e) => {
        setIsTouched(true);
    };

    const reset = () => {
        setEnteredValue('');
        setIsTouched(false);
    };

    useEffect(() => {
        getInputData({ value: enteredValue, reset, isValid: valueIsValid })
    }, [enteredValue, valueIsValid, getInputData]);


    const inputClasses = hasError ? `${s.input} ${s.invalid}` : `${s.input}`;
    return (
        <Wrapper>
            <input
                type="text"
                className={inputClasses}
                onChange={onValueChangeHandler}
                value={enteredValue}
                onInput={onInputHandler}
                placeholder='What needs to be done?'
            />
            {hasError && <p className={s.error}>Task length must be at least 1 symbol</p>}
        </Wrapper>
    )
} 