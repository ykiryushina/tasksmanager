import { useCallback } from 'react';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { tasksActions } from '../store/tasks-slice';
import { Input } from './Input/Input';

export const TasksForm = () => {
    const [inputData, setInputData] = useState({});
    const dispatch = useDispatch();

    const addTask = () => {
        dispatch(tasksActions.addTask({
            id: Date.now(),
            text: inputData.value
        }));
    };

    const getInputData = useCallback((data) => {
        setInputData(data);
    }, [setInputData]);

    const submitHandler = (e) => {
        e.preventDefault();
        if (inputData.isValid) {
            addTask();
            inputData.reset();
        }
    };

    return (
        <form onSubmit={submitHandler}>
            <Input getInputData={getInputData} />
        </form>
    )
}