import { memo } from 'react';
import s from './Button.module.css';

export const Button = memo(({ text, onClick, isActive }) => {
    const buttonClasses = isActive ? `${s.button} ${s.active}` : `${s.button}`;

    return (
        <button className={buttonClasses} onClick={onClick} >{text}</button>
    )
}) 