import { tasksActions } from "./tasks-slice";

const url = 'https://react-http-2dc58-default-rtdb.europe-west1.firebasedatabase.app/tasks.json';

export const saveTask = (tasks, activeTasks) => {
    return async () => {
        const sendRequest = async () => {
            await fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ tasks: tasks, activeTasks: activeTasks })
            })
        }
        try {
            await sendRequest();
        } catch (error) {
            return error;
        }
    }
};

export const getAllTasks = () => {
    return async (dispatch) => {
        const fetchData = async () => {
            const response = await fetch('https://react-http-2dc58-default-rtdb.europe-west1.firebasedatabase.app/tasks.json');
            let data = await response.json();
            if (!response.ok) {
                throw new Error(data.message);
            }
            return data = { ...data };
        };

        try {
            const tasks = await fetchData();
            dispatch(tasksActions.replaceTasks({ tasks: tasks.tasks || [], activeTasks: tasks.activeTasks }));
        } catch (error) {
            return error;
        }
    }
}