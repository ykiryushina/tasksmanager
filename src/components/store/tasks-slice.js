import { createSlice } from '@reduxjs/toolkit';

export const tasksSlice = createSlice({
    name: 'tasks',
    initialState: {
        items: [],
        currentItems: [],
        activeTasks: 0,
        isFiltered: false,
        isActiveFilterActive: false,
        isCompletedFilterActive: false
    },
    reducers: {
        replaceTasks(state, action) {
            console.log(action.payload);
            state.currentItems = action.payload.tasks;
            state.items = action.payload.tasks;
            state.activeTasks = action.payload.activeTasks; //undefined
        },

        addTask(state, action) {
            const newItem = action.payload;
            state.activeTasks++;
            state.items.push({
                id: newItem.id,
                text: newItem.text,
                isCompleted: false
            });
            if (state.isActiveFilterActive) {
                state.currentItems = state.items.filter(item => item.isCompleted === false);
                state.isFiltered = false;
            } else if (state.isCompletedFilterActive) {
                state.currentItems = state.items.filter(item => item.isCompleted === true);
            } else {
                state.currentItems = [...state.items];
            };
        },

        completeTask(state, action) {
            const chosenTask = state.items.find(item => item.id === action.payload);
            const currentTask = state.currentItems.find(item => item.id === chosenTask.id);
            chosenTask.isCompleted = !chosenTask.isCompleted;
            currentTask.isCompleted = !currentTask.isCompleted;

            if (state.isActiveFilterActive) {
                state.currentItems = state.items.filter(item => item.isCompleted === false);
                state.isFiltered = true;
            } else if (state.isCompletedFilterActive) {
                state.currentItems = state.items.filter(item => item.isCompleted === true);;
            }

            if (!chosenTask.isCompleted && !currentTask.isCompleted) {
                state.activeTasks++;
            }
            if (state.activeTasks !== 0 && chosenTask.isCompleted && currentTask.isCompleted) {
                state.activeTasks--;
            };
        },

        clearCompleted(state) {
            state.items = state.items.filter(item => item.isCompleted === false);
            if (state.isCompletedFilterActive) {
                state.currentItems = state.items.filter(item => item.isCompleted === true);
            } else {
                state.currentItems = [...state.items];
            };
        },

        filterTasks(state, action) {
            switch (action.payload) {
                case 'active':
                    state.currentItems = state.items.filter(item => item.isCompleted === false);
                    state.isCompletedFilterActive = false;
                    state.isActiveFilterActive = true;
                    state.isFiltered = true;
                    break;
                case 'completed':
                    state.isCompletedFilterActive = true;
                    state.isActiveFilterActive = false;
                    state.currentItems = state.items.filter(item => item.isCompleted === true);
                    state.isFiltered = true;
                    break;
                default:
                    state.isCompletedFilterActive = false;
                    state.isActiveFilterActive = false;
                    state.currentItems = [...state.items];
                    state.isFiltered = false;
                    break;
            }
        }
    }
});

export const tasksActions = tasksSlice.actions;